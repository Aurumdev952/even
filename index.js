function check(num) {
    if (num % 2 === 0) {
        return true;
    } else {
        return false;
    }
}

function even(num, multi=false) {
    if (multi) {
        let result = {}
        num.forEach(n => {
            result[n.toString()] = check(n)
        })
    } else {
        return check(num)
    }
}

export default even